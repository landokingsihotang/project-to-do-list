<?php

use Illuminate\Support\Facades\Route;
use app\Http\Controllers\RegisterController;
use app\Http\Controllers\LoginController;
use App\Http\Controllers\TodoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->middleware('guest');

Route::get('/login', function () {return view('sign');})->name('login')->middleware('guest');

Route::post('/login', 'App\Http\Controllers\LoginController@login')->name('logins')->middleware('guest');
Route::post('/register', 'App\Http\Controllers\RegisterController@register')->name('register')->middleware('guest');
Route::get('/register', function () {return view('sign');})->name('regist')->middleware('guest');
Route::post('/logout', 'App\Http\Controllers\LoginController@logout')->name('logout');



Route::get('/todos','App\Http\Controllers\TodoController@index')->name('todos.index')->middleware('auth');
Route::get('/todos/create','App\Http\Controllers\TodoController@create')->name('todos.create')->middleware('auth');
Route::post('/todos/store', 'App\Http\Controllers\TodoController@store')->name('todos.store')->middleware('auth');
Route::get('/todos/{todo}/edit', 'App\Http\Controllers\TodoController@edit')->name('todos.edit')->middleware('auth');
Route::put('/todos/{todo}/update','App\Http\Controllers\TodoController@update')->name('todos.update')->middleware('auth');
Route::delete('/todos/{todo}/destroy','App\Http\Controllers\TodoController@destroy')->name('todos.destroy')->middleware('auth');
Route::put('/todos/{todo}/checked', 'App\Http\Controllers\TodoController@checked')->name('todos.checked')->middleware('auth');

Route::get('/histories','App\Http\Controllers\HistoryController@index')->name('history.index');
Route::delete('/history/{history}', 'App\Http\Controllers\HistoryController@destroy')->name('history.destroy');

// Route::get('/','App\Http\Controllers\HistoryController@destroy')->name('history.destroy');
// Route::get('/profil', function () {return view('profil/index');})->name('profil')->middleware('auth');
Route::get('/profil','App\Http\Controllers\ProfilController@index')->name('profil.index')->middleware('auth');
Route::patch('/profil','App\Http\Controllers\ProfilController@update')->name('profil')->middleware('auth');
