@extends('todos.layout')

@section('style')
<link rel="stylesheet" href="{{ asset('css/create.css') }}">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
@endsection

@section('title', 'Tambah Todo')

@section('content')
    <form class="todo-form" action="{{ route('todos.store') }}" method="post">
        @csrf
        <label for="activity">Aktivitas:</label>
        <input type="text" id="activity" name="activity" required>

        <label for="deadline">Deadline:</label>
        <input type="text" id="deadline" name="deadline" class="flatpickr">

        <button type="submit">Tambah</button>
    </form>

   
@endsection
 {{-- <a href="{{ route('todos.index') }}">Kembali ke Daftar Todo</a> --}}