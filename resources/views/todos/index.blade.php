@extends('todos.layout')

@section('style')
  <link rel="stylesheet" href="{{ asset('css/indextodo.css') }}">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

@endsection
@section('content')

@isset($user)
@forelse ($user->todos as $todo)
<div class="col-span-1">
    <div class="p-4 border-2 border-gray-200 border-dashed rounded-lg dark:border-gray-700" style="background-color: {{ $todo->completed ? '#ffb380' : '#b3d9ff' }};">
        <div style="word-wrap: break-word;">
            <span class="fs-5 urbanist-font">{{ $todo->activity }}</span>
        </div>
        <div class="flex flex-col items-start mt-4">
            <div class="flex items-center urbanist-font">
                <span class="urbanist-font deadline-color">
                    Deadline: <br>
                    {{ \Carbon\Carbon::parse($todo->deadline)->format('D, d F Y') }} <br>
                    {{ \Carbon\Carbon::parse($todo->deadline)->format('H.i') }}
                </span>
                <form action="{{ route('todos.checked', $todo) }}" method="post" class="ml-4">
                    @csrf
                    @method('put')
                    <input class="form-check-input" type="checkbox" name="completed" id="completed" {{ $todo->completed ? 'checked' : '' }} onchange="this.form.submit()" />
                </form>
            </div>
            <div class="flex items-center mt-2 urbanist-font">
                <form action="{{ route('todos.edit', $todo) }}" method="get">
                    <button type="submit" class="btn {{ $todo->completed ? 'btn-primary-light' : 'btn-primary' }} mr-2">Edit</button> <!-- Ganti warna tombol Edit -->
                </form>
                <form action="{{ route('todos.destroy', $todo) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn {{ $todo->completed ? 'btn-danger-light' : 'btn-danger' }}" style="{{ $todo->completed ? 'border:1px solid salmon' : ' ' }} ; font-weight:semi bold" >Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
@empty
<div class="col-span-3 urbanist-font">
    <p>List Tidak Ditemukan.</p>
</div>
@endforelse
            @else
                <div class="col-span-3 urbanist-font">
                    <p>List Tidak Ditemukan.</p>
                </div>
            @endisset

{{-- <form action="{{ route('todos.create') }}" method="get">
    <button type="submit" class="btn btn-success" style="background-color: #28a745; border-color: #28a745; color: #fff;">Tambah</button>
</form> --}}

@endsection
@section('submit')
<form 
 action="{{ route('todos.create') }}" method="get">
    <button type="submit" class="btn btn-success urbanist-font data">Tambah</button>
</form>

@endsection