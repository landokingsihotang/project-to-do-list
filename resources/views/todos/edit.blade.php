@extends('todos.layout')

@section('title', 'Edit Todo')

@section('content')
    {{-- <h1>Edit Todo</h1> --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">


    <form action="{{ route('todos.update', $todo) }}" method="post">
        @csrf
        @method('PUT')

        <div class="mb-4">
            <label for="activity" class="block text-sm font-medium text-gray-700 dark:text-gray-300">Aktivitas:</label>
            <input type="text" id="activity" name="activity" value="{{ $todo->activity }}" class="mt-1 p-2 w-full border rounded-md shadow-sm focus:outline-none focus:border-blue-500 dark:bg-gray-800 dark:border-gray-600 dark:text-white" required>
        </div>

        <div class="mb-4">
            <label for="deadline" class="block text-sm font-medium text-gray-700 dark:text-gray-300">Deadline:</label>
            <input type="text" id="deadline" name="deadline" value="{{ $todo->deadline }}" class="mt-1 p-2 w-full border rounded-md shadow-sm focus:outline-none focus:border-blue-500 dark:bg-gray-800 dark:border-gray-600 dark:text-white flatpickr">
        </div>

        <button type="submit" class="px-4 py-2 bg-blue-500 text-white rounded-md hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Simpan Perubahan</button>
    </form>


    {{-- <a href="{{ route('todos.index') }}">Kembali ke Daftar Todo</a> --}}
@endsection
