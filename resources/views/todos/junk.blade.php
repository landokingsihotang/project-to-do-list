{{-- <body class="antialiased">
    <a href="{{ route('history.index') }}" class="btn btn-info">Lihat Riwayat</a>
        @if (Route::has('login') | Route::has('register'))
            <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right z-10">
                @auth
                    <form action="{{ route('logout') }}" method="post">
                        @csrf
                        <button type="submit">Logout</button>
                    </form>
                @endauth
            </div>
        @endif

    <h1>To-do List</h1> --}}

    <ul>
        @isset($user)
            @forelse ($user->todos as $todo)
                <li class="d-flex justify-content-between py-3">
                    <span class="fs-4">{{ $todo->activity }}</span>
                    <div class="d-flex gap-4">
                        <span>Deadline: {{ $todo->deadline }}</span>
                    <form action="{{ route('todos.checked', $todo) }}" method="post" class="d-flex align-items-center gap-4">
                        @csrf
                        @method('put')
                        <input class="form-check-input" type="checkbox" name="completed" id="completed" {{ $todo->completed ? 'checked' : '' }} onchange="this.form.submit()" />
                    </form>
                    <form action="{{ route('todos.edit', $todo) }}" method="get" style="display: inline;">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </form>
                        <form action="{{ route('todos.destroy', $todo) }}" method="post" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                </li>
            @empty
                <li>List Tidak Ditemukan.</li>
            @endforelse
        @else
            <li>List Tidak Ditemukan.</li>
        @endisset
    </ul>

    <form action="{{ route('todos.create') }}" method="get">
        <button type="submit" class="btn btn-success">Tambah</button>
    </form>
{{-- </body> --}}


{{-- <a href="{{ route('history.index') }}" class="btn btn-info">Lihat Riwayat</a> --}}
{{-- @if (Route::has('login') | Route::has('register'))
    <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right z-10">
        @auth
            <form action="{{ route('logout') }}" method="post">
                @csrf
                <button type="submit">Logout</button>
            </form>
        @endauth
    </div>
@endif --}}