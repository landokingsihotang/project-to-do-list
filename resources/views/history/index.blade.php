@extends('todos.layout')

@section('style')
<style>.bg-gray-light {
    background-color: #366ad0; /* Ganti dengan warna abu-abu light yang diinginkan */
}
.a{
    font-family: 'urbanist';
    font-weight:600;}

</style>
@endsection
@section('content')
<ul class="col-span-3">
    @isset($histories)
    @forelse ($histories as $history)
        <li class="p-3 mb-1 mr-6 border rounded-lg bg-white dark:bg-gray-800 dark:border-gray-700">
            <div class="flex items-start justify-between">
                <div class="flex items-center gap-2">
                    @if ($history->action === 'create')
                        <span class="a text-green-500">
                           {{ $history->user->name }} Membuat List Baru: {{ $history->activity }}
                        </span>
                    @elseif ($history->action === 'update')
                        <span class="a text-yellow-500">
                           {{ $history->user->name }} Mengupdate List: {{ $history->activity }}
                        </span>
                    @elseif ($history->action === 'delete')
                        <span class="a text-red-500">
                           {{ $history->user->name }} Menghapus: {{ $history->activity }}
                        </span>
                    @elseif ($history->action === 'complete')
                        <span class="a text-blue-500">
                           {{ $history->user->name }} Menyelesaikan: {{ $history->activity }}
                        </span>
                    @endif
                </div>
                <div class="flex items-center justify-end gap-2">
                    <span class="text-gray-500">
                        ( Dibuat pada {{ $history->created_at->format('Y/m/d H:i') }} )
                    </span>
                    <form action="{{ route('history.destroy', $history) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="a text-red-500 hover:underline focus:outline-none">Delete</button>
                    </form>
                </div>
            </div>
        </li>
    @empty
        <li class="py-3">Riwayat Tidak Ditemukan.</li>
    @endforelse
@else
    <li class="py-3">Riwayat Tidak Ditemukan.</li>
@endisset


</ul>


    @endsection

