<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Urbanist:wght@400;700&display=swap">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <style>
.navbar {
    background: linear-gradient(to right, #2563eb, #f0f0f0);
      padding-left: 15px; /* Navbar top and bottom padding */
      padding-top: 0; /* Resetting top padding */
      box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1); /* Optional: add a subtle box shadow */
      display: flex;
      justify-content: space-between;
    }

    .navbar-brand img {
      height: 50px;
      border-radius: 50%;
    }

    /* Navbar links */
    .navbar-nav .nav-link {
      color: #ffffff; /* Navbar link text color */
      margin-right: 15px; /* Navbar link right margin */
      background-color: #2563eb;
      /* padding: 10px 15px; */
      transition: all 300ms ease-in;
    }

    /* Active link */
    .navbar-nav .nav-item.active .nav-link {
      color: #ffffff; /* Active link text color */
    }

    /* Right-aligned links */
    .navbar-nav.ml-auto {
      display: flex;
      align-items: center;
    }

    .navbar-nav.ml-auto .nav-item {
      margin-left: 15px;
    }

    .nav-link {
      display: inline-block;
      padding: 5px 15px;
      text-align: center;
      text-decoration: none;
      color: #ffffff; /* Warna teks */
      font-weight: bold; /* Ketebalan teks */
      border: 2px solid #007bff; /* Warna dan ketebalan border */
      border-radius: 5px; /* Radius border */
      transition: color 0.3s, background-color 0.3s; /* Animasi perubahan warna */
    }

    .nav-link:hover {
        color: #2563eb; /* Warna biru tua */
        background-color:rgb(255, 255, 255);
    }

   

    @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&family=Roboto:wght@400;700;900&display=swap");

    html, body {
      height: 100%;
      margin: 0;
      font-family: "Roboto", sans-serif;
    }

    section {
      position: relative;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
      min-height: min(89vh, 600px);
      background-color: #8583b7;
      margin: 0;
      padding: 1rem;
      text-align: center;
      overflow: hidden;

      &:before {
        position: absolute;
        mix-blend-mode: overlay;
        filter: brightness(70%);
        content: "";
        inset: 0;
        width: 100%;
        height: 100%;
        background: url("https://images.unsplash.com/photo-1582005450386-52b25f82d9bb?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2940&q=80");
        background-size: cover;
        background-position: center;
      }
    }

    h1, h2 {
      margin-top: 2rem;
      color: white;
    }

    h1 {
      position: relative;
      font-weight: 900;
      font-size: clamp(2.5rem, 5vw, 4rem);

      div {
        color: #ddd6fe;
      }
    }

    h2 {
      font-size: clamp(1.3rem, 2vw, 3rem);
    }

    p {
      margin-top: 1rem;
      font-size: clamp(1.3rem, 3vw, 4rem);
      color: white;
    }

    .cta {
      position: relative;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
      margin-top: 3rem;
      gap: 1.5rem;
    }

    .cta button {
      border: none;
      padding: 1rem 3rem;
      font-size: clamp(1.1rem, 1.3vw, 3rem);
      border-radius: 8px;
      cursor: pointer;
    }


  button:first-of-type {
    background-color: #2563eb;
    color: #ffffff;
    transition: all 150ms ease-in;

    &:hover {
      background-color: #ffffff;
      color: #2563eb;
    }
  }

        </style>
    <title>Document</title>
</head><body>
    <nav class="navbar navbar-expand-lg d-flex justify-content-between">
        <a class="navbar-brand" href="#" style="color:white;font-family:'urbanist';font-weight:550;font-size:21px">
            <img src="{{ asset('img/Frame.png') }}" alt="Logo" class="img-fluid rounded-circle " style="height: 50px;border : 1px solid;border-color:#f0f0f0">
        QuickAgenda</a>
        
        <div class="d-flex align-items-center">
            <ul class="navbar-nav ml-auto text-center">
                <li class="nav-item">
                    <a class="nav-link" href="/login" style=" font-family: 'Urbanist', sans-serif; margin-right: -2px;">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}" style=" font-family: 'Urbanist', sans-serif;">Register</a>
                </li>
            </ul>
        </div>
        
    </nav>

    <section>
    <h1>Catat Jadwalmu</h1>
    <p>Tak ada Kata Lupa.</p>

    <div class="cta">
        <button id="btnLogin">Get started</button>
    </div>
</section>

<script>
    document.getElementById('btnLogin').addEventListener('click', function() {
        // Redirect ke halaman login
        window.location.href = "{{ route('login') }}";
    });
</script>

  </body>
</html>