@extends('todos.layout')

@section('style')
<style>

.a {
  box-shadow: 1px 4px 8px 1px rgba(0, 0, 0, 0.1); /* Ganti dengan warna bayangan yang diinginkan */
  background-color:#ffb380;/* Ganti dengan warna latar belakang yang diinginkan */
  border-radius: 20px;
}
.block{
    font-family: 'urbanist';
    font-weight:700;
    color:#ffb380;
}
.mt-1{
    font-family: 'urbanist';
}
.border-custom {
        border-width: 2px;
        border-style: solid;
        border-color: #ffb380; /* Change the color as per your preference */
    }

    /* Add some padding and border-radius for a rounded appearance */
    .rounded-custom {
        border-radius: 0.375rem; /* You can adjust the radius based on your preference */
    }

    /* Style the focus state for better visibility */
    .focus-custom {
        box-shadow: 0 0 0 3px rgb(252, 235, 235); 
    }

    /* Add these styles to your existing styles or create a new section in your stylesheet */
.bg-salmon {
    background-color: #f56a0d;
    transition: background-color 0.3s ease-in-out;
}

button:hover {
    background-color: lightcoral;
}

/* Efek klik */
button:active,
button:focus {
    background-color: lightcoral;
    outline: none; /* Hilangkan garis focus bawaan browser saat tombol diklik */
}
.rounded-md.resize-none{
    font-family: 'urbanist';
}

</style>
<!-- Tambahkan stylesheet dan script flatpickr -->
@endsection
@section('content')

<div class="grid grid-cols-1 gap-4 mx-auto mt-5 shadow-md a">
    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRoq5qnjxWx-4yrVhZD10316bKU88QLnDn0ZA&usqp=CAU" alt="User Image" class="w-32 h-32 rounded-full shadow-md p-6">
</div>

<div class="grid grid-cols-1 gap-1 mx-auto mt-1">
    {{-- Div untuk informasi publik pengguna --}}
    {{-- {{ route('update-profile') }} --}}
    {{-- {{ $userData->email }} --}}
            @isset($user)

                {{-- Form untuk mengubah informasi publik pengguna --}}
                <form action="{{ route('profil') }}" method="post">
                    @csrf
                    @method('patch')
        
                    <label for="username" class="block mt-4 text-sm font-medium text-gray-700">Username</label>
                    <input type="text" id="username" name="username" value="{{ old('username', optional($user->profil)->username) }}" class="mt-1 p-2 border-custom rounded-custom focus:outline-none focus:border-indigo-500 focus-custom bg-gray-100 text-gray-700 placeholder-gray-500" required>

                    {{-- Nama Pengguna --}}
                    <label for="name" class="block mt-4 text-sm font-medium text-gray-700">Nama Pengguna</label>
                    <input type="text" id="name" name="name" value="{{ old('name', $user->name) }}" class="mt-1 p-2 border-custom rounded-custom focus:outline-none focus:border-indigo-500 focus-custom bg-gray-100 text-gray-700 placeholder-gray-500" required>
        
                    {{-- Email --}}
                    <label for="email" class="block mt-4 text-sm font-medium text-gray-700">Email</label>
                    <input type="email" id="email" name="email" value="{{ old('email', $user->email) }}" class="mt-1 p-2 border-custom rounded-custom focus:outline-none focus:border-indigo-500 focus-custom bg-gray-100 text-gray-700 placeholder-gray-500" required>
        
                    <label for="job" class="block mt-4 text-sm font-medium text-gray-700">Kesibukan</label>
                    <input type="text" id="job" name="kesibukan" value="{{ old('job', optional($user->profil)->kesibukan) }}" class="mt-1 p-2 border-custom rounded-custom focus:outline-none focus:border-indigo-500 focus-custom bg-gray-100 text-gray-700 placeholder-gray-500" required>
        
                    <label for="tanggal_lahir" class="block mt-4 text-sm font-medium text-gray-700">Tanggal Lahir</label>
                    {{-- <input type="date" id="tanggal_lahir" name="tanggal_lahir" value="{{ old('tanggal_lahir', optional($user->profil)->tanggal_lahir) }}" class="mt-1 p-2 border-custom rounded-custom focus:outline-none focus:border-indigo-500 focus-custom bg-gray-100 text-gray-700 placeholder-gray-500" required> --}}
                    <input type="text" id="tanggal_lahir" name="tanggal_lahir" value="{{ old('tanggal_lahir', optional($user->profil)->tanggal_lahir) }}" class="mt-1 p-2 border-custom rounded-custom focus:outline-none focus:border-indigo-500 focus-custom bg-gray-100 text-gray-700 placeholder-gray-500" required>

                    {{-- Tombol Simpan --}}
                    <button type="submit" class="p-2 bg-salmon text-white rounded-md hover:bg-light-salmon focus:outline-none focus:bg-light-salmon" >Simpan</button>
                </form>
            @endisset
     
    </div>

    <div class="grid grid-cols-1 gap-4 mt-5">
        <textarea class="border border-gray-300 focus:border-indigo-500 p-2 rounded-md resize-none w-60 h-24 transition-all duration-300 ease-in-out focus:outline-none focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="Tulis harapan Anda..."></textarea>
    </div>
    
@endsection