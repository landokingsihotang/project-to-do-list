/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./resources/views/history/index.blade.php",

    "./resources/views/todos/index.blade.php",
    "./resources/views/todos/create.blade.php",
    "./resources/views/todos/edit.blade.php",
    "./resources/views/todos/layout.blade.php",
    
    "./resources/views/dashboard.blade.php",
    "./resources/views/layout.blade.php",
    "./resources/views/sign.blade.php",
    "./resources/views/welcomes.blade.php",

    "./public/css/style.css",
    "./public/css/todos.css",

    "./public/js/script.js",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
