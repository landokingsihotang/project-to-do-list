<?php

namespace App\Http\Controllers;

use App\Models\History;
use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class TodoController extends Controller
{
    public function index()
{
    $user = Auth::user();
    $todos = Todo::all();
    return view('todos.index', compact('user', 'todos'));
}


    public function create()
    {
        return view('todos.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'activity' => 'required|string',
            'deadline' => 'date',
        ]);

        $userId = Auth::id(); // Mengambil id user yang sedang login

        Todo::create([
        'user_id' => $userId,
        'activity' => $request->activity,
        'deadline' => $request->deadline,
    ]);

        $this->recordHistory($userId, $request->activity, 'create');
        return redirect()->route('todos.index');
    }

    public function edit(Todo $todo)
    {
        return view('todos.edit', compact('todo'));
    }

    public function update(Request $request, Todo $todo)
    {
        $request->validate([
            'activity' => 'required|string',
            'deadline' => 'date',
        ]);

        $todo->update([
            'activity' => $request->activity,
            'deadline' => $request->deadline,
        ]);

        $this->recordHistory($todo->user_id, $todo->activity, 'update');

        return redirect()->route('todos.index');
    }

    public function destroy(Todo $todo)
    {

        $this->recordHistory($todo->user_id, $todo->activity, 'delete');
        $todo->delete();

        return redirect()->route('todos.index');
    }

    // public function checked(Todo $todo)
    // {   
    //     $todo->update([
    //         'completed' => !$todo->completed,
    //     ]);

    //     return Redirect::to(route('todos.index'));
    // }

    public function checked(Todo $todo)
{   
    if (!$todo->completed) {
        // Jika tugas sebelumnya belum selesai
        $todo->update([
            'completed' => true,
        ]);

        // Membuat riwayat "complete"
        $this->recordHistory($todo->user_id, $todo->activity, 'complete');
    } else {
        // Jika tugas sebelumnya sudah selesai
        $this->deleteHistory($todo->user_id, $todo->activity, 'complete');
        
        $todo->update([
            'completed' => false,
        ]);

    }

    return Redirect::to(route('todos.index'));
}

private function deleteHistory($userId, $activity, $action)
{
    // Menghapus riwayat berdasarkan kondisi tertentu
    History::where('user_id', $userId)
        ->where('activity', $activity)
        ->where('action', $action)
        ->delete();
}

    private function recordHistory($userId, $activity, $action)
    {
        History::create([
            'user_id' => $userId,
            'activity' => $activity,
            'action' => $action,
        ]);
    }
}

