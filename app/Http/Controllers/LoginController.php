<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
//     public function login(Request $request){
//         $this->validate($request, ['email' => 'required', 'password' => 'required'], ['email.required' => 'Email Wajib Di isi', 'password.required' => 'Password Wajib Di isi']);

//     $infologin = [
//         'email' => $request->email,
//         'password'=> $request->password
//     ];

//     if(Auth::attempt($infologin)){
//         return "sukses";
//     }
//     else {
//         return "Gagal";
//     }
// }
public function showLoginForm()
{
    return view('index');
}

public function login(Request $request)
{
    // Validasi data formulir
    $this->validate($request, [
        'email' => 'required|string|email',
        'password' => 'required|string'
    ]);

    $infologin = [
                'email' => $request->email,
                'password'=> $request->password
            ];

    if (Auth::attempt($infologin)) {
        $request->session()->regenerate();
 
        return redirect()->route('todos.index');
    
    // ->with('success', 'Login berhasil!');
    } 
    else {
    $email = $request->input('email');
    return redirect('/login')->with('error', 'Gagal login. Periksa email dan kata sandi Anda.')->withInput(['email' => $email]);
}
}

public function logout(Request $request)
{
    Auth::logout();
 
    $request->session()->invalidate();
 
    $request->session()->regenerateToken();
 
    return redirect('/login');
}  
}


