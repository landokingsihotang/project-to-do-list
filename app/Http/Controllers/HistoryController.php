<?php
// app/Http/Controllers/HistoryController.php

namespace App\Http\Controllers;

use App\Models\History;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $histories = $user->history; // Mengambil riwayat yang terkait dengan pengguna yang sedang masuk
        return view('history.index', compact('user', 'histories'));
    }
    

    public function destroy(History $history)
    {
        $history->delete();
        return redirect()->route('history.index');
    }
}
