<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProfilController extends Controller
{
    public function index()
    {
        $user = auth()->user(); // atau Auth::user();
        return view('profil.index', compact('user'));
    }
    

    public function update(Request $request)
{
    $user = Auth::user();

    $request->validate([
        'username' => 'required|unique:profiles,username,' . $user->id,
        'kesibukan' => 'nullable|string',
        'tanggal_lahir' => 'nullable|date',
    ]);

    DB::table('users')->where('id', $user->id)->update([
        'name' => $request->input('name'),
        'email' => $request->input('email'),
    ]);

    if ($user->profil) {
        $user->profil->update([
            'username' => $request->input('username'),
            'kesibukan' => $request->input('kesibukan'),
            'tanggal_lahir' => $request->input('tanggal_lahir'),
        ]);
    } else {
        $user->profil()->create([
            'username' => $request->input('username'),
            'kesibukan' => $request->input('kesibukan'),
            'tanggal_lahir' => $request->input('tanggal_lahir'),
        ]);
    }
    

    return redirect()->route('profil')->with('success', 'Profil berhasil diperbarui!');
}


}
