<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function register(Request $request)
{
    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
        // 'username' => 'required|string|max:255|unique:users',
        'email' => 'required|email',
        'password' => 'required|min:8|'
    ]);

    if ($validator->fails()) {
        return redirect('/login') 
            ->withErrors($validator)
            ->withInput()
            ->with('error', 'Registrasi gagal! Silakan periksa kembali formulir Anda.');
    }
    User::create([
        'name' => $request['name'],
        'email' => $request['email'],
        'password' => Hash::make($request['password']),
    ]);

    return redirect()->route('todos.index');

}
}
