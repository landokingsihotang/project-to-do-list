<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class sendNotif extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public $todo;

    public function __construct($todo)
    {
        $this->todo = $todo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
{
    return (new MailMessage)
        ->subject('Task Deadline Reminder')
        ->line('The deadline for the following task is less than 1 day:')
        ->line('Task: ' . $this->todo->activity)
        ->line('Deadline: ' . Carbon::parse($this->todo->deadline)->format('Y-m-d H:i:s'))
        ->line('Thank you!');
}


    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
