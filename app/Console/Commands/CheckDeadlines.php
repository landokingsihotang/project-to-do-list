<?php

namespace App\Console\Commands;

use App\Models\Todo;
use App\Notifications\sendNotif;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckDeadlines extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:check-deadlines';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $todos = Todo::whereDate('deadline', '=', Carbon::now()->addDay()->toDateString())
                    ->get();

        foreach ($todos as $todo) {
            $user = $todo->user;

            // Menggunakan notifikasi
            $user->notify(new sendNotif($todo));
    }
}}
