<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'activity', 'completed', 'deadline'];

    // Jika Anda ingin menggunakan atribut tanggal (timestamps)
    public $timestamps = true;

    // Nilai default untuk atribut completed
    protected $attributes = [
        'completed' => false,
    ];

    // Relasi dengan model User, satu todo dimiliki oleh satu user
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function history()
    {
        return $this->belongsTo(History::class);
    }
}

