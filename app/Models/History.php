<?php
// app/Models/History.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{

    protected $fillable = ['user_id', 'activity', 'action'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
