<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// Di dalam model Profile
class Profil extends Model
{
    protected $fillable = ['username','tanggal_lahir','kesibukan',];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    
}
